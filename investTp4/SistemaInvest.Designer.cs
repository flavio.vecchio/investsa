﻿namespace investTp4
{
    partial class SistemaInvest
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dgInversores = new System.Windows.Forms.DataGridView();
            this.dgAccionesPorInversor = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.dgAcciones = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbTotalClienteComun = new System.Windows.Forms.TextBox();
            this.tbTotalPremiunHasta = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tbTotalPremiunSobre = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tbTotal = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAltaInversor = new System.Windows.Forms.Button();
            this.btnModificacionInversor = new System.Windows.Forms.Button();
            this.btnBajaInversor = new System.Windows.Forms.Button();
            this.btnBajaAccion = new System.Windows.Forms.Button();
            this.btnModificacionAccion = new System.Windows.Forms.Button();
            this.btnAltaAccion = new System.Windows.Forms.Button();
            this.btnComprar = new System.Windows.Forms.Button();
            this.btnVender = new System.Windows.Forms.Button();
            this.btOrdenarPorNombre = new System.Windows.Forms.Button();
            this.btOrdenaInversorLegajo = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgInversores)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAccionesPorInversor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAcciones)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Inversores";
            // 
            // dgInversores
            // 
            this.dgInversores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgInversores.Location = new System.Drawing.Point(60, 68);
            this.dgInversores.MultiSelect = false;
            this.dgInversores.Name = "dgInversores";
            this.dgInversores.ReadOnly = true;
            this.dgInversores.RowHeadersVisible = false;
            this.dgInversores.RowHeadersWidth = 82;
            this.dgInversores.RowTemplate.Height = 33;
            this.dgInversores.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgInversores.Size = new System.Drawing.Size(606, 248);
            this.dgInversores.TabIndex = 1;
            this.dgInversores.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgInversores_CellClick);
            // 
            // dgAccionesPorInversor
            // 
            this.dgAccionesPorInversor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAccionesPorInversor.Location = new System.Drawing.Point(813, 68);
            this.dgAccionesPorInversor.MultiSelect = false;
            this.dgAccionesPorInversor.Name = "dgAccionesPorInversor";
            this.dgAccionesPorInversor.ReadOnly = true;
            this.dgAccionesPorInversor.RowHeadersVisible = false;
            this.dgAccionesPorInversor.RowHeadersWidth = 82;
            this.dgAccionesPorInversor.RowTemplate.Height = 33;
            this.dgAccionesPorInversor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAccionesPorInversor.Size = new System.Drawing.Size(606, 248);
            this.dgAccionesPorInversor.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(808, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(218, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Acciones del inversor";
            // 
            // dgAcciones
            // 
            this.dgAcciones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAcciones.Location = new System.Drawing.Point(60, 405);
            this.dgAcciones.MultiSelect = false;
            this.dgAcciones.Name = "dgAcciones";
            this.dgAcciones.ReadOnly = true;
            this.dgAcciones.RowHeadersVisible = false;
            this.dgAcciones.RowHeadersWidth = 82;
            this.dgAcciones.RowTemplate.Height = 33;
            this.dgAcciones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgAcciones.Size = new System.Drawing.Size(606, 248);
            this.dgAcciones.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 361);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Acciones";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(806, 351);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(363, 84);
            this.label4.TabIndex = 6;
            this.label4.Text = "Total recaudado por \r\nclientes comunes";
            // 
            // tbTotalClienteComun
            // 
            this.tbTotalClienteComun.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbTotalClienteComun.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotalClienteComun.Location = new System.Drawing.Point(1217, 368);
            this.tbTotalClienteComun.Name = "tbTotalClienteComun";
            this.tbTotalClienteComun.ReadOnly = true;
            this.tbTotalClienteComun.Size = new System.Drawing.Size(202, 49);
            this.tbTotalClienteComun.TabIndex = 7;
            // 
            // tbTotalPremiunHasta
            // 
            this.tbTotalPremiunHasta.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbTotalPremiunHasta.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotalPremiunHasta.Location = new System.Drawing.Point(1217, 470);
            this.tbTotalPremiunHasta.Name = "tbTotalPremiunHasta";
            this.tbTotalPremiunHasta.ReadOnly = true;
            this.tbTotalPremiunHasta.Size = new System.Drawing.Size(202, 49);
            this.tbTotalPremiunHasta.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(806, 453);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(363, 126);
            this.label5.TabIndex = 8;
            this.label5.Text = "Total recaudado por \r\nclientes premiun\r\nhasta 20000";
            // 
            // tbTotalPremiunSobre
            // 
            this.tbTotalPremiunSobre.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbTotalPremiunSobre.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotalPremiunSobre.Location = new System.Drawing.Point(1217, 621);
            this.tbTotalPremiunSobre.Name = "tbTotalPremiunSobre";
            this.tbTotalPremiunSobre.ReadOnly = true;
            this.tbTotalPremiunSobre.Size = new System.Drawing.Size(202, 49);
            this.tbTotalPremiunSobre.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(806, 604);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(363, 126);
            this.label6.TabIndex = 10;
            this.label6.Text = "Total recaudado por \r\nclientes premiun\r\nsobre 20000";
            // 
            // tbTotal
            // 
            this.tbTotal.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTotal.Location = new System.Drawing.Point(1217, 766);
            this.tbTotal.Name = "tbTotal";
            this.tbTotal.ReadOnly = true;
            this.tbTotal.Size = new System.Drawing.Size(202, 49);
            this.tbTotal.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.875F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(806, 766);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(236, 42);
            this.label7.TabIndex = 12;
            this.label7.Text = "Total general";
            // 
            // btnAltaInversor
            // 
            this.btnAltaInversor.Location = new System.Drawing.Point(183, 24);
            this.btnAltaInversor.Name = "btnAltaInversor";
            this.btnAltaInversor.Size = new System.Drawing.Size(75, 38);
            this.btnAltaInversor.TabIndex = 14;
            this.btnAltaInversor.Text = "A";
            this.btnAltaInversor.UseVisualStyleBackColor = true;
            this.btnAltaInversor.Click += new System.EventHandler(this.btnAltaInversor_Click);
            // 
            // btnModificacionInversor
            // 
            this.btnModificacionInversor.Location = new System.Drawing.Point(288, 24);
            this.btnModificacionInversor.Name = "btnModificacionInversor";
            this.btnModificacionInversor.Size = new System.Drawing.Size(75, 38);
            this.btnModificacionInversor.TabIndex = 15;
            this.btnModificacionInversor.Text = "M";
            this.btnModificacionInversor.UseVisualStyleBackColor = true;
            this.btnModificacionInversor.Click += new System.EventHandler(this.btnModificacionInversor_Click);
            // 
            // btnBajaInversor
            // 
            this.btnBajaInversor.Location = new System.Drawing.Point(400, 24);
            this.btnBajaInversor.Name = "btnBajaInversor";
            this.btnBajaInversor.Size = new System.Drawing.Size(75, 38);
            this.btnBajaInversor.TabIndex = 16;
            this.btnBajaInversor.Text = "B";
            this.btnBajaInversor.UseVisualStyleBackColor = true;
            this.btnBajaInversor.Click += new System.EventHandler(this.btnBajaInversor_Click);
            // 
            // btnBajaAccion
            // 
            this.btnBajaAccion.Location = new System.Drawing.Point(400, 351);
            this.btnBajaAccion.Name = "btnBajaAccion";
            this.btnBajaAccion.Size = new System.Drawing.Size(75, 38);
            this.btnBajaAccion.TabIndex = 19;
            this.btnBajaAccion.Text = "B";
            this.btnBajaAccion.UseVisualStyleBackColor = true;
            this.btnBajaAccion.Click += new System.EventHandler(this.btnBajaAccion_Click);
            // 
            // btnModificacionAccion
            // 
            this.btnModificacionAccion.Location = new System.Drawing.Point(288, 351);
            this.btnModificacionAccion.Name = "btnModificacionAccion";
            this.btnModificacionAccion.Size = new System.Drawing.Size(75, 38);
            this.btnModificacionAccion.TabIndex = 18;
            this.btnModificacionAccion.Text = "M";
            this.btnModificacionAccion.UseVisualStyleBackColor = true;
            this.btnModificacionAccion.Click += new System.EventHandler(this.btnModificacionAccion_Click);
            // 
            // btnAltaAccion
            // 
            this.btnAltaAccion.Location = new System.Drawing.Point(183, 351);
            this.btnAltaAccion.Name = "btnAltaAccion";
            this.btnAltaAccion.Size = new System.Drawing.Size(75, 38);
            this.btnAltaAccion.TabIndex = 17;
            this.btnAltaAccion.Text = "A";
            this.btnAltaAccion.UseVisualStyleBackColor = true;
            this.btnAltaAccion.Click += new System.EventHandler(this.btnAltaAccion_Click);
            // 
            // btnComprar
            // 
            this.btnComprar.Location = new System.Drawing.Point(591, 354);
            this.btnComprar.Name = "btnComprar";
            this.btnComprar.Size = new System.Drawing.Size(75, 38);
            this.btnComprar.TabIndex = 20;
            this.btnComprar.Text = "C";
            this.btnComprar.UseVisualStyleBackColor = true;
            this.btnComprar.Click += new System.EventHandler(this.btnComprar_Click);
            // 
            // btnVender
            // 
            this.btnVender.Location = new System.Drawing.Point(1344, 12);
            this.btnVender.Name = "btnVender";
            this.btnVender.Size = new System.Drawing.Size(75, 38);
            this.btnVender.TabIndex = 21;
            this.btnVender.Text = "V";
            this.btnVender.UseVisualStyleBackColor = true;
            this.btnVender.Click += new System.EventHandler(this.btnVender_Click);
            // 
            // btOrdenarPorNombre
            // 
            this.btOrdenarPorNombre.Location = new System.Drawing.Point(672, 68);
            this.btOrdenarPorNombre.Name = "btOrdenarPorNombre";
            this.btOrdenarPorNombre.Size = new System.Drawing.Size(75, 38);
            this.btOrdenarPorNombre.TabIndex = 22;
            this.btOrdenarPorNombre.Text = "ON";
            this.btOrdenarPorNombre.UseVisualStyleBackColor = true;
            this.btOrdenarPorNombre.Click += new System.EventHandler(this.btOrdenarPorNombre_Click);
            // 
            // btOrdenaInversorLegajo
            // 
            this.btOrdenaInversorLegajo.Location = new System.Drawing.Point(672, 130);
            this.btOrdenaInversorLegajo.Name = "btOrdenaInversorLegajo";
            this.btOrdenaInversorLegajo.Size = new System.Drawing.Size(75, 38);
            this.btOrdenaInversorLegajo.TabIndex = 23;
            this.btOrdenaInversorLegajo.Text = "OL";
            this.btOrdenaInversorLegajo.UseVisualStyleBackColor = true;
            this.btOrdenaInversorLegajo.Click += new System.EventHandler(this.btOrdenaInversorLegajo_Click);
            // 
            // SistemaInvest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1502, 842);
            this.Controls.Add(this.btOrdenaInversorLegajo);
            this.Controls.Add(this.btOrdenarPorNombre);
            this.Controls.Add(this.btnVender);
            this.Controls.Add(this.btnComprar);
            this.Controls.Add(this.btnBajaAccion);
            this.Controls.Add(this.btnModificacionAccion);
            this.Controls.Add(this.btnAltaAccion);
            this.Controls.Add(this.btnBajaInversor);
            this.Controls.Add(this.btnModificacionInversor);
            this.Controls.Add(this.btnAltaInversor);
            this.Controls.Add(this.tbTotal);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.tbTotalPremiunSobre);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbTotalPremiunHasta);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbTotalClienteComun);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dgAcciones);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dgAccionesPorInversor);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dgInversores);
            this.Controls.Add(this.label1);
            this.Name = "SistemaInvest";
            this.Text = "Sistema Invest";
            this.Load += new System.EventHandler(this.SistemaInvest_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgInversores)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAccionesPorInversor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgAcciones)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgInversores;
        private System.Windows.Forms.DataGridView dgAccionesPorInversor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgAcciones;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbTotalClienteComun;
        private System.Windows.Forms.TextBox tbTotalPremiunHasta;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tbTotalPremiunSobre;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbTotal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnAltaInversor;
        private System.Windows.Forms.Button btnModificacionInversor;
        private System.Windows.Forms.Button btnBajaInversor;
        private System.Windows.Forms.Button btnBajaAccion;
        private System.Windows.Forms.Button btnModificacionAccion;
        private System.Windows.Forms.Button btnAltaAccion;
        private System.Windows.Forms.Button btnComprar;
        private System.Windows.Forms.Button btnVender;
        private System.Windows.Forms.Button btOrdenarPorNombre;
        private System.Windows.Forms.Button btOrdenaInversorLegajo;
    }
}

