﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace investTp4
{
   
    public partial class SistemaInvest : Form
    {
        private int contador=0;
        IComparer<Modelo.Accion> _sortAccion;
        IComparer<InversorVista> _sortInversor;
        private int sortOrderInversor;
        IComparer<Modelo.Inversion> _sortInversion;

        public SistemaInvest()
        {
            InitializeComponent();
            _sortInversor = new Modelo.CriteriosDeComparacion.NombreAsc();
            sortOrderInversor = 0;
        }

        private void SistemaInvest_Load(object sender, EventArgs e)
        {
            refrescarPantallas();
        }

        private void refrescarPantallas()
        {
            refrescarPantallaInversores();
            refrescarPantallaAccionesDelInversor();
            refrescarPantallaAcciones();
            tbTotalClienteComun.Text = Modelo.SistemaInvest.getInstance().TotalPorClienteComun.ToString();
            tbTotalPremiunHasta.Text = Modelo.SistemaInvest.getInstance().TotalPorClientePremiunHasta.ToString();
            tbTotalPremiunSobre.Text = Modelo.SistemaInvest.getInstance().TotalPorClientePremiunDesde.ToString();
            tbTotal.Text = Modelo.SistemaInvest.getInstance().TotalComisiones().ToString();
        }

        private void refrescarPantallaInversores()
        {
            int index = this.obtenerIndice(dgInversores);
            List<Modelo.Inversor> inversores = Modelo.SistemaInvest.getInstance().Inversores;
            
            List<InversorVista> result = new List<InversorVista>();

            //Se necesita la clase vista para poder ordenar usando la forma propuesta
            //y no las alternativas que ofrece .NET
            foreach (Modelo.Inversor _i in inversores)
            { result.Add(new InversorVista(_i.Nombre, _i.Apellido, _i.Dni, _i.Legajo)); }

            /*var result = Modelo.SistemaInvest.getInstance().Inversores.Select(r => new
            {
                Legajo = r.Legajo,
                Nombre = r.Nombre,
                Apellido = r.Apellido,
                DNI = r.Dni
            }).ToList();
            */

            result.Sort(_sortInversor);
            dgInversores.DataSource = result;
            this.seleccionarFilaActual(dgInversores, index, result.Count);
        }

        private void refrescarPantallaAcciones()
        {
            int index = this.obtenerIndice(dgAcciones);
            var result = Modelo.SistemaInvest.getInstance().Acciones.Select(r => new
            {
                Codigo = r.codigoFormateado(),
                Denominacion = r.Denominacion,
                Cotizacion = r.Cotizacion,
                Emision = r.CantidadEmitida
            }).ToList();

            dgAcciones.DataSource = result;
            this.seleccionarFilaActual(dgAcciones, index, result.Count);
        }

        private string obtenerCampoDeDgSeleccionado(DataGridView dg,string campo)
        {
            DataGridViewRow row = dg.CurrentRow;
            if (row == null)
            {
                return "";
            }
            string result = row.Cells[campo].Value.ToString();

            return result;
        }

        private string obtenerCodigoDeAccionSeleccionado()
        {
            return this.obtenerCampoDeDgSeleccionado(dgAcciones, "Codigo");
        }
        private string obtenerLegajoDeInversorSeleccionado()
        {
            return this.obtenerCampoDeDgSeleccionado(dgInversores, "Legajo");
        }

        private void refrescarPantallaAccionesDelInversor()
        {
            string legajo = this.obtenerLegajoDeInversorSeleccionado();
            if (legajo.Equals(""))
            {
                return;
            }

            int index = this.obtenerIndice(dgAccionesPorInversor);

            var result = Modelo.SistemaInvest.getInstance().obtenerInversionesDeInversorConLegajo(legajo).Select(r => new
            {
                Codigo = r.codigoDeAccion(),
                Denominacion = r.denominacionDeAccion(),
                Cantidad = r.cantidadInvertida(),
                ValorTotal = r.totalInvertido()
            }).ToList();

            dgAccionesPorInversor.DataSource = result;
            this.seleccionarFilaActual(dgAccionesPorInversor, index, result.Count);
        }

        private void btnAltaInversor_Click(object sender, EventArgs e)
        {
            contador++;
            string legajo = contador.ToString();
            string nombre = "unNombreComun" + contador.ToString();
            string apellido = "unApellidoComun" + contador.ToString();
            string dni = contador.ToString();

            Modelo.SistemaInvest.getInstance().agregarInversorComun(legajo,nombre,apellido,dni);

            contador++;
             legajo = contador.ToString();
             nombre = "unNombrePremiun" + contador.ToString();
             apellido = "unApellidoPremiun" + contador.ToString();
             dni = contador.ToString();

            Modelo.SistemaInvest.getInstance().agregarInversorPremiun(legajo,nombre,apellido,dni);

            refrescarPantallas();

        }

        private void btnModificacionInversor_Click(object sender, EventArgs e)
        {
            string legajo = this.obtenerLegajoDeInversorSeleccionado();
            if (legajo.Equals(""))
            {
                return;
            }

            contador++;
            string nombre = "unNombreModificado" + contador.ToString();
            string apellido = "unApellidoModificado" + contador.ToString();
            string dni = contador.ToString();

            Modelo.SistemaInvest.getInstance().modificarInversorPorLegajo(legajo, nombre, apellido, dni);

            refrescarPantallas();
        }

        private void btnBajaInversor_Click(object sender, EventArgs e)
        {
            string legajo = this.obtenerLegajoDeInversorSeleccionado();
            if (legajo.Equals(""))
            {
                return;
            }

            Modelo.SistemaInvest.getInstance().eliminarInversorPorLegajo(legajo);

            refrescarPantallas();
        }

        private void btnAltaAccion_Click(object sender, EventArgs e)
        {
            contador++;
            string codigo = "EMPR"+contador.ToString()+"-10"+contador.ToString() +"-A0B0";
            string denominacion = "unaDenominacion" + contador.ToString();
            float cotizacion = 10;
            float cantidadEmitida = 50000;

            Modelo.SistemaInvest.getInstance().agregarAccion(codigo,denominacion,cotizacion,cantidadEmitida);

            refrescarPantallas();
        }

        private void btnModificacionAccion_Click(object sender, EventArgs e)
        {
            string codigo = this.obtenerCodigoDeAccionSeleccionado();
            if (codigo.Equals(""))
            {
                return;
            }

            contador++;
            string denominacion = "unaDenominacion" + contador.ToString();
            float cotizacion = 20;
            float cantidadEmitida = 120000;

            Modelo.SistemaInvest.getInstance().modificarAccionPorCodigo(codigo, denominacion, cotizacion, cantidadEmitida);

            refrescarPantallas();

        }

        private void btnBajaAccion_Click(object sender, EventArgs e)
        {
            string codigo = this.obtenerCodigoDeAccionSeleccionado();
            if (codigo.Equals(""))
            {
                return;
            }

            Modelo.SistemaInvest.getInstance().eliminarAccionPorCodigo(codigo);

            refrescarPantallas();
        }

        private void btnComprar_Click(object sender, EventArgs e)
        {
            string legajo = this.obtenerLegajoDeInversorSeleccionado();
            string codigo = this.obtenerCodigoDeAccionSeleccionado();

            try
            {
                Modelo.SistemaInvest.getInstance().comprarAccionParaInversor(codigo, legajo, 30000);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            

            refrescarPantallas();
        }

        private void btnVender_Click(object sender, EventArgs e)
        {
            string legajo = this.obtenerLegajoDeInversorSeleccionado();
            string codigo = this.obtenerCodigoDeInversionSeleccionado();
            try
            {
                Modelo.SistemaInvest.getInstance().venderAccionParaInversor(codigo, legajo, 30000);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
           
            refrescarPantallas();
        }

        private string obtenerCodigoDeInversionSeleccionado()
        {
            return this.obtenerCampoDeDgSeleccionado(dgAccionesPorInversor, "Codigo");
        }

        private void dgInversores_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            refrescarPantallas();
        }

        internal int obtenerIndice(DataGridView dg)
        {
            int index = -1;
            if (dg.CurrentRow != null)
            {
                index = dg.CurrentRow.Index;
            }

            return index;
        }

        internal void seleccionarFilaActual(DataGridView dg, int index, int count)
        {
            if (count < index - 1) { index = -1; }
            if (count == index) { index = index - 1; }
            if (count == 0) { index = -1; }
            if (index >= 0)
            {
                dg.CurrentCell = dg[0, index];
            }
        }

        private void btOrdenarPorNombre_Click(object sender, EventArgs e)
        {
            if (sortOrderInversor == 0)
            {
                _sortInversor = new Modelo.CriteriosDeComparacion.NombreDesc();
                sortOrderInversor = 1;
            }
            else
            {
                _sortInversor = new Modelo.CriteriosDeComparacion.NombreAsc();
                sortOrderInversor = 0;
            }
            refrescarPantallas();
        }

        private void btOrdenaInversorLegajo_Click(object sender, EventArgs e)
        {
            if (sortOrderInversor == 0)
            {
                _sortInversor = new Modelo.CriteriosDeComparacion.LegajoAsc();
                sortOrderInversor = 1;
            }
            else
            {
                _sortInversor = new Modelo.CriteriosDeComparacion.LegajoDesc();
                sortOrderInversor = 0;
            }
            refrescarPantallas();
        }
    }
}
