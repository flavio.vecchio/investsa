﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4
{
    class InversorVista
    {
        private string legajo;
        private string nombre;
        private string apellido;
        private string dni;
        

        public InversorVista(string nombre, string apellido, string dni, string legajo)
        {
            Nombre = nombre;
            Apellido = apellido;
            Dni = dni;
            Legajo = legajo;
        }
        //Esto da el orden de las columnas
        public string Legajo { get => legajo; set => legajo = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Dni { get => dni; set => dni = value; }
        
    }
}
