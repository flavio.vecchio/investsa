﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public class CodigoDeAccion : Top
    {
        private string codigoEmpresa;
        private string codigoValidacion;
        private string codigoInterno;


        public CodigoDeAccion(string codigo)
        {
            validarGuiones(codigo);

            string[] codigos = codigo.Split('-');

            codigoEmpresa = codigos[0];
            codigoValidacion = codigos[1];
            codigoInterno = codigos[2];

            validarFormatoDeCodigoDeValidacion();
            validarFormatoDeCodigoInterno();

        }

        private void validarFormatoDeCodigoDeValidacion()
        {
            bool esNumerico = int.TryParse(codigoValidacion, out int n);
            if (!esNumerico)
            {
                throw new DatosInvalidosException("El codigo de validacion debe ser numerico");
            }

            if (codigoInterno.Length != 4)
            {
                throw new DatosInvalidosException("El codigo interno debe ser de 4 lugares");
            }
        }

        private void validarFormatoDeCodigoInterno()
        {
            string letras = codigoInterno.Substring(0, 1) + codigoInterno.Substring(2, 1);
            string numeros = codigoInterno.Substring(1, 1) + codigoInterno.Substring(3, 1);
            if (!letras.All(Char.IsLetter))
            {
                throw new DatosInvalidosException("El primer y tercer caracter debe ser letra");
            }
            if (!numeros.All(Char.IsDigit))
            {
                throw new DatosInvalidosException("El segundo y cuatro caracter debe ser numero");
            }
        }

        private static void validarGuiones(string codigo)
        {
            var count = codigo.Count(x => x == '-');
            if (count != 2)
            {
                throw new DatosInvalidosException("El codigo debe tener 2 guiones");
            }
        }

        public override string ToString()
        {
            return codigoEmpresa + "-" + codigoValidacion + "-" + codigoInterno; 
        }
        ~CodigoDeAccion() { }

    }
}
