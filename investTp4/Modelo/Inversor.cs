﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace investTp4.Modelo
{
    public class Inversor: Top
    {
        private string legajo;
        private string apellido;
        private string nombre;
        private string dni;
        private TipoDeInversor tipoInversor;
        private List<Inversion> inversiones;
        

        internal Inversion obtenerInversionParaAccion(Accion a)
        {
            Inversion i = Inversiones.Find(x => x.Accion == a);
            if(i == null)
            {
                i = new Inversion(a,this);
                //Me subscribo al evento
                a.cambioCotizacion += new EventHandler<CambioCotizacion>(muestraCambioCotizacion);
                Inversiones.Add(i);
            }

            return i;
        }

        private void muestraCambioCotizacion(object sender, CambioCotizacion e)
        {
            //Cuando se activa el evento hago esto
            MessageBox.Show("Soy el inversor "+Nombre+": "+e.ToString());
        }

        internal void eliminarInversion(Inversion inversion)
        {
            inversiones.Remove(inversion);
            inversion.eliminar();
        }

        internal float cobrarComisionPor(float totalInvertido)
        {
            return tipoInversor.calcularComisionPor(totalInvertido);
        }

        public string Legajo { get => legajo; set => legajo = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public string Dni { get => dni; set => dni = value; }
        public TipoDeInversor TipoInversor { get => tipoInversor; set => tipoInversor = value; }
        public List<Inversion> Inversiones { get => inversiones; set => inversiones = value; }

        public Inversor(string legajo, string apellido, string nombre, string dni, string tipo)
        {
            Legajo = legajo ?? throw new ArgumentNullException(nameof(legajo));
            Apellido = apellido ?? throw new ArgumentNullException(nameof(apellido));
            Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
            Dni = dni ?? throw new ArgumentNullException(nameof(dni));
            Inversiones = new List<Inversion>();

            if (tipo == "comun")
            {
                TipoInversor = new InversorComun();
            }
            else
            {
                TipoInversor = new InversorPremiun();
            }
        }

        ~Inversor()
        {

        }

        internal void modificarDatos(string nombre, string apellido, string dni)
        {
            Nombre = nombre;
            Apellido = apellido;
            Dni = dni;
        }

        internal void eliminar()
        {
            foreach (Inversion i in Inversiones)
            {
                i.eliminar();
            }
            Inversiones = null;
        }

        internal void eliminarInversionConAccion(Accion a)
        {
            Inversion[] invs = new Inversion[inversiones.Count];

            inversiones.CopyTo(invs);

            foreach (Inversion i in invs)
            {
                i.eliminarInversionConAccion(a);
            }
        }

        
    }
}
