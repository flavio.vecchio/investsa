﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public class InversorComun: TipoDeInversor
    {

        public override float calcularComisionPor(float totalInvertido)
        {
            float comision = totalInvertido * (TipoDeInversor.comision / 100f);
            SistemaInvest.getInstance().sumarATotalClienteComun(comision);

            return comision;
        }


        ~InversorComun()
        {

        }
    }
}
