﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    [Serializable()]

    public class RegistroDuplicadoException : System.Exception

    {

        public RegistroDuplicadoException() : base() { }

        public RegistroDuplicadoException(string message) : base(message) { }

        public RegistroDuplicadoException(string message, System.Exception inner) : base(message, inner) { }



        // A constructor is needed for serialization when an

        // exception propagates from a remoting server to the client.

        protected RegistroDuplicadoException(System.Runtime.Serialization.SerializationInfo info,

            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }

    }



    public class DatosInvalidosException : System.Exception
    {

        public DatosInvalidosException() : base() { }

        public DatosInvalidosException(string message) : base(message) { }

        public DatosInvalidosException(string message, System.Exception inner) : base(message, inner) { }



        // A constructor is needed for serialization when an

        // exception propagates from a remoting server to the client.

        protected DatosInvalidosException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

}
