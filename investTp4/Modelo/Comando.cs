﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public abstract class Comando:Top
    {
        internal float cobrarComisionAInversor(Inversor i, float totalInvertido)
        {
            return i.cobrarComisionPor(totalInvertido);
        }
    }
}
