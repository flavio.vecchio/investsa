﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public class InversorPremiun: TipoDeInversor
    {

        public override float calcularComisionPor(float totalInvertido)
        {
            //Valores no deben estar harcodeados
            if(totalInvertido < 20000)
            {
                float comision = totalInvertido * (TipoDeInversor.comision / 100f);
                SistemaInvest.getInstance().sumarATotalClientePremiunHasta(comision);
                return comision;
            }
            float hasta = 20000;
            float diferencia = totalInvertido - hasta;
            float comisionHasta = hasta * (TipoDeInversor.comision / 100f);
            float comisionDesde = diferencia * (0.005f);
            float total = comisionHasta + comisionDesde;

            SistemaInvest.getInstance().sumarATotalClientePremiunHasta(comisionHasta);
            SistemaInvest.getInstance().sumarATotalClientePremiunDesde(comisionDesde);

            return total;
        }

        ~InversorPremiun()
        {

        }
    }
}
