﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public class SistemaInvest: Top
    {
        private List<Accion> acciones;
        private List<Inversor> inversores;
        static SistemaInvest instance;
        private float totalPorClienteComun;
        private float totalPorClientePremiunHasta;
        private float totalPorClientePremiunDesde;

        internal void sumarATotalClientePremiunHasta(float comision)
        {
            totalPorClientePremiunHasta = totalPorClientePremiunHasta + comision;
        }

        internal void sumarATotalClienteComun(float comision)
        {
            totalPorClienteComun = totalPorClienteComun + comision;
        }

        

        public List<Accion> Acciones { get => acciones; set => acciones = value; }
        public List<Inversor> Inversores { get => inversores; set => inversores = value; }

        internal void sumarATotalClientePremiunDesde(float comisionDesde)
        {
            totalPorClientePremiunDesde = totalPorClientePremiunDesde + comisionDesde;
        }

        internal float TotalComisiones()
        {
            return totalPorClienteComun + totalPorClientePremiunDesde + totalPorClientePremiunHasta;
        }

        public static SistemaInvest Instance { get => instance; set => instance = value; }
        public float TotalPorClienteComun { get => totalPorClienteComun; set => totalPorClienteComun = value; }
        public float TotalPorClientePremiunHasta { get => totalPorClientePremiunHasta; set => totalPorClientePremiunHasta = value; }
        public float TotalPorClientePremiunDesde { get => totalPorClientePremiunDesde; set => totalPorClientePremiunDesde = value; }

        public SistemaInvest()
        {
            acciones = new List<Accion>();
            inversores = new List<Inversor>();
        }

        static public SistemaInvest getInstance()
        {
            if (Instance == null)
            {
                Instance = new SistemaInvest();
            }

            return Instance;
        }

        ~SistemaInvest()
        {

        }

        internal List<Inversion> obtenerInversionesDeInversorConLegajo(string legajo)
        {
            Inversor i = this.obtenerInversorConLegajo(legajo);

            if (i == null) { return new List<Inversion>(); }

            return i.Inversiones;

        }

        internal void agregarInversorComun(string legajo, string nombre, string apellido, string dni)
        {
            Inversor i = new Inversor(legajo, apellido, nombre, dni, "comun");
            if (this.obtenerInversorConLegajo(legajo) != null)
            {
                throw new DatosInvalidosException("Ya existe un inversor con ese legajo");
            }
            Inversores.Add(i);
        }

        internal void agregarInversorPremiun(string legajo, string nombre, string apellido, string dni)
        {
            Inversor i = new Inversor(legajo, apellido, nombre, dni, "premiun");
            Inversores.Add(i);
        }

        internal void modificarInversorPorLegajo(string legajo, string nombre, string apellido, string dni)
        {
            Inversor i = this.obtenerInversorConLegajo(legajo);
            validarObjeto(i);
            i.modificarDatos(nombre, apellido, dni);
        }

        private Inversor obtenerInversorConLegajo(string legajo)
        {
            Inversor i = Inversores.Find(x => x.Legajo == legajo);
            return i;
        }

        internal void eliminarInversorPorLegajo(string legajo)
        {
            Inversor i = this.obtenerInversorConLegajo(legajo);
            validarObjeto(i); 
            i.eliminar();
            Inversores.Remove(i);
        }

        internal void agregarAccion(string codigo, string denominacion, float cotizacion, float cantidadEmitida)
        {
            Accion a = new Accion(codigo, denominacion, cotizacion, cantidadEmitida);
            if (this.obtenerAccionConCodigo(codigo) != null)
            {
                throw new DatosInvalidosException("Ya existe una accion con ese codigo");
            }
            Acciones.Add(a);
        }

        private Accion obtenerAccionConCodigo(string codigo)
        {
            Accion a = Acciones.Find(x => x.codigoFormateado() == codigo);
            return a;
        }

        internal void modificarAccionPorCodigo(string codigo, string denominacion, float cotizacion, float cantidadEmitida)
        {
            Accion a = this.obtenerAccionConCodigo(codigo);
            validarObjeto(a);
            a.modificarDatos(denominacion, cotizacion, cantidadEmitida);

            
        }

        internal void eliminarAccionPorCodigo(string codigo)
        {
            Accion a = this.obtenerAccionConCodigo(codigo);
            validarObjeto(a);
            foreach (Inversor i in Inversores)
            {
                i.eliminarInversionConAccion(a);
            }
            a.eliminar();
            Acciones.Remove(a);
           


        }

        internal void comprarAccionParaInversor(string codigo, string legajo, float cantidad)
        {
            
                Accion a = this.obtenerAccionConCodigo(codigo);
                Inversor i = this.obtenerInversorConLegajo(legajo);
                validarObjeto(a);
                validarObjeto(i);
                Comprar comando = new Comprar();
                comando.ejecutar(i, a, cantidad);
            
            
           
        }

        internal void venderAccionParaInversor(string codigo, string legajo, float cantidad)
        {
           
                Inversor i = this.obtenerInversorConLegajo(legajo);
                Accion a = this.obtenerAccionConCodigo(codigo);
                validarObjeto(a);
                validarObjeto(i);
                Inversion iv = i.obtenerInversionParaAccion(a);

                Vender comando = new Vender();
                comando.ejecutar(i, a, cantidad);
            
            
        }

        
    }
}
