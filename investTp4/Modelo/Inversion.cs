﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public class Inversion: Top
    {
        private Accion accion;
        private float cantidad;
        private Inversor _inversor;
        

        public Inversion(Accion a,Inversor i)
        {
            this.accion = a;
            _inversor = i;
            cantidad = 0;
            
        }

        public Accion Accion { get => accion; set => accion = value; }

        internal void eliminarSiEsPosible()
        {
            if (cantidad <= 0)
            {
                _inversor.eliminarInversion(this);
            }
        }

        public float Cantidad { get => cantidad; set => cantidad = value; }

        ~Inversion()
        {

        }

        internal string codigoDeAccion()
        {
            return accion.codigoFormateado();
        }

        internal string denominacionDeAccion()
        {
            return accion.Denominacion;
        }

        internal float cantidadInvertida()
        {
            return Cantidad;
        }

        internal float totalInvertido()
        {
            return Cantidad * accion.Cotizacion;
        }

        internal void eliminar()
        {
            accion.devolverCantidad(Cantidad);
            accion = null;

        }

        internal void eliminarInversionConAccion(Accion a)
        {
            if (accion == a)
            {
                _inversor.eliminarInversion(this);
            }
        }
    }
}
