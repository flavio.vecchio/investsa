﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    class CriteriosDeComparacion : Top
    {
      
        public class NombreAsc : IComparer<InversorVista>

        {

            public int Compare(InversorVista x, InversorVista y)

            {

                return String.Compare(x.Nombre, y.Nombre);

            }

        }

        public class NombreDesc : IComparer<InversorVista>

        {

            public int Compare(InversorVista x, InversorVista y)

            {

                return String.Compare(x.Nombre, y.Nombre) * -1;

            }

        }

        public class LegajoAsc : IComparer<InversorVista>

        {

            public int Compare(InversorVista x, InversorVista y)

            {

                return String.Compare(x.Legajo, y.Legajo);

            }

        }

        public class LegajoDesc : IComparer<InversorVista>

        {

            public int Compare(InversorVista x, InversorVista y)

            {

                return String.Compare(x.Legajo, y.Legajo) * -1;

            }

        }

    }
}
