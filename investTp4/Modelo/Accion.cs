﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public class Accion: Top
    {
        private CodigoDeAccion codigo;
        private string denominacion;
        private float cotizacion;
        private float cantidadEmitida;
        public event EventHandler<CambioCotizacion> cambioCotizacion;

        internal void validarVentaDeAcciones(float cantidad)
        {
            if (cantidad > this.cantidadEmitida)
            {
                throw new DatosInvalidosException("No se puede vender porque supera la cantidad emitida");
            }
        }

        internal void vender(float cantidad)
        {
            cantidadVendida = cantidadVendida - cantidad;
        }

        private float cantidadVendida;

        internal void validarCompraDeAcciones(float cantidad)
        {
            if (cantidad > this.cantidadDisponibleParaVenta())
            {
                throw new DatosInvalidosException("No se puede comprar porque supera la cantidad disponible");
            }
        }

        internal void comprar(float cantidad)
        {
            cantidadVendida = cantidadVendida + cantidad;
        }

        private float cantidadDisponibleParaVenta()
        {
            return cantidadEmitida - cantidadVendida;
        }

        internal string codigoFormateado()
        {
            return codigo.ToString();
        }

        
        public string Denominacion { get => denominacion; set => denominacion = value; }

        internal void devolverCantidad(float cantidad)
        {
            CantidadVendida = CantidadVendida - cantidad;
        }

        public float Cotizacion { get => cotizacion; set => cotizacion = value; }
        public float CantidadEmitida { get => cantidadEmitida; set => cantidadEmitida = value; }
        public float CantidadVendida { get => cantidadVendida; set => cantidadVendida = value; }

        public Accion(string codigo, string denominacion, float cotizacion, float cantidadEmitida)
        {
            this.codigo = new CodigoDeAccion(codigo);
            this.denominacion = denominacion;
            this.cotizacion = cotizacion;
            this.cantidadEmitida = cantidadEmitida;
            this.cantidadVendida = 0;
        }

        ~Accion()
        {

        }

        internal void modificarDatos(string denominacion, float cotizacion, float cantidadEmitida)
        {
            if (Cotizacion != cotizacion)
            {
                //Genero el evento
                cambioCotizacion?.Invoke(this, new CambioCotizacion("Se cambio la cotizacion"));
            }
            Denominacion = denominacion;
            Cotizacion = cotizacion;
            CantidadEmitida = cantidadEmitida;
        }

        internal void eliminar()
        {
            codigo = null;
        }
    }

    //Este es el evento
    public class CambioCotizacion : EventArgs
    {
        string _evento;
        public CambioCotizacion(string mensaje)
        { _evento = mensaje; }

        public override string ToString()
        {
            return _evento;
        }
    }
}

