﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public class Comprar: Comando
    {

        ~Comprar() { }

        internal float ejecutar(Inversor i, Accion a, float cantidad)
        {
            try
            {
                a.validarCompraDeAcciones(cantidad);
                Inversion iv = i.obtenerInversionParaAccion(a);
                iv.Cantidad = iv.Cantidad + cantidad;
                a.comprar(cantidad);
                return this.cobrarComisionAInversor(i, cantidad * a.Cotizacion);
            }
            catch (Exception)
            {

                throw new DatosInvalidosException("No se puede realizar la compra");
            }
            


        }

       
    }
}
