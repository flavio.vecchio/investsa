﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace investTp4.Modelo
{
    public class Vender:Comando
    {

        ~Vender() { }

        internal float ejecutar(Inversor i, Accion a, float cantidad)
        {
            try
            {
                a.validarVentaDeAcciones(cantidad);
                Inversion iv = i.obtenerInversionParaAccion(a);
                iv.Cantidad = iv.Cantidad - cantidad;
                a.vender(cantidad);

                float comision = this.cobrarComisionAInversor(i, cantidad * a.Cotizacion);
                iv.eliminarSiEsPosible();
                return comision;
            }
            catch (Exception)
            {

                throw new DatosInvalidosException("No se puede ejecutar la venta");
            }
            


        }
    }
}
